#ifndef ADC_H_
#define ADC_H_


#include "Arduino.h"
#include <SPI.h>


class ADC_SPI {

public:
    /** ADC resolution in bits. */
    static const uint8_t ADC_RES_BITS = 12;
    /** ADC resolution. */
    static const uint16_t ADC_RES = (1 << ADC_RES_BITS);

    static const int16_t ADC_COUNTS_HIGH = ADC_RES*0.55;
    static const int16_t ADC_COUNTS_LOW = ADC_RES*0.45;

    /**
    * The ADC has 8 inputs, usable as 8 individually single inputs with
    * reference to analog ground or 4 differential inputs. The polarity
    * of each differential input can be configured.
    *
    * bits|  4  | 3 | 2 | 1
    * :--:|:---:|:-:|:-:|:-:
    *  -  |type |ch |ch |ch
    *
    *  -  |      description
    * :--:|:--------------------------
    * type| 1 bit for single(1) or differential(0) input
    * ch  | 3 bit for channel number selection
    */
    enum Channel {
        SINGLE_0 = 0b1000,  /**< single channel 0 */
        SINGLE_1 = 0b1001,  /**< single channel 1 */
        SINGLE_2 = 0b1010,  /**< single channel 2 */
        SINGLE_3 = 0b1011,  /**< single channel 3 */
        SINGLE_4 = 0b1100,  /**< single channel 4 */
        SINGLE_5 = 0b1101,  /**< single channel 5 */
        SINGLE_6 = 0b1110,  /**< single channel 6 */
        SINGLE_7 = 0b1111,  /**< single channel 7 */
        DIFF_0PN = 0b0000,  /**< differential channel 0 (input 0+,1-) */
        DIFF_0NP = 0b0001,  /**< differential channel 0 (input 0-,1+) */
        DIFF_1PN = 0b0010,  /**< differential channel 1 (input 2+,3-) */
        DIFF_1NP = 0b0011,  /**< differential channel 1 (input 2-,3+) */
        DIFF_2PN = 0b0100,  /**< differential channel 2 (input 4+,5-) */
        DIFF_2NP = 0b0101,  /**< differential channel 2 (input 5-,5+) */
        DIFF_3PN = 0b0110,  /**< differential channel 3 (input 6+,7-) */
        DIFF_3NP = 0b0111   /**< differential channel 3 (input 6-,7+) */
    };

    /**
    * Initiates a MCP3208 object. The chip select pin must be already
    * configured as output.
    * @param [in] vref ADC reference voltage in mV.
    * @param [in] csPin pin number to use for chip select.
    * @param [in] spi reference to the SPI interface to use.
    */
    ADC_SPI(uint8_t csPin, SPIClass *spi);

    /**
    * Initiates a MCP3208 object. The chip select pin must be already
    * configured as output. The default SPI interface will be used for
    * communication.
    * @param [in] vref the ADC reference voltage in mV.
    * @param [in] csPin the pin number to use for chip select.
    */
    ADC_SPI(uint8_t csPin);

    void init();

    /**
    * Calibrates read timing using the supplied channel. A calibration
    * should be performed after evey SPI frequency changes or other events
    * that could have an impact on the sampling speed.
    * The SPI interface must be initialized and put in a usable state
    * before calling this function.
    */
    void calibrate(Channel ch);

    /**
    * Reads the supplied channel. The SPI interface must be initialized and
    * put in a usable state before calling this function.
    * @param [in] ch defines the channel to read from.
    * @return the converted raw value.
    */
    uint16_t read(Channel ch) const;

    /**
    * Returns the corrrected Vcc value from reading the 2.5 V Vref
    * @return float value of Vcc
    */
    float getVcc();

private:
    /**
    * Defines 16 bit SPI data. The structure implements an easy
    * access to each byte.
    */
    union SpiData {
        uint16_t value;  /**< value */
        struct {
            uint8_t loByte;  /**< low byte */
            uint8_t hiByte;  /**< high byte */
        };
    };


    /**
    * Creates command data from the supplied channel.
    * @param [in] ch the channel to create the command for.
    * @return the SPI data.
    */
    static SpiData createCmd(Channel ch);

    /**
    * Transfers the supplied SPI data command.
    * @param [in] cmd the SPI data command to transfer.
    * @return the ADC value from the SPI response.
    */
    uint16_t transfer(SpiData cmd) const;

private:
    static const long ADC_CLK = 1600000;  // SPI clock 1.6MHz

    uint16_t mVref;
    float mVcc;
    uint8_t mCsPin;
    uint32_t mSplSpeed;
    SPIClass *mSpi;
};

#endif
