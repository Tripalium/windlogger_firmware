/**
*******************************************************************************
*******************************************************************************
*
*	License :
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*     any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
*
*******************************************************************************
*******************************************************************************
*
*
*    @file   FSM.h
*    @author gilou
*    @date   19 févr. 2018
*    @brief  The FSM is the finish state machine mechanism.
*
*    This is the Final State Machine organization
*
*/


// Basic Includes
#include "Arduino.h"    // The general arduino commands which are compatible with the Windlogger
#include "FSM.h"        // The FSM header with all the class definitions of the final state machine of the Windlogger

// Hardware includes
#include "Rtc_Pcf8563.h"
#include "SD.h"
#include "commons.h"


/******************************************************************************
* Constructor and destructor definition
*/
FSM::FSM() {
}	// FSM constructor


FSM::~FSM() {}	// FSM destructor

/******************************************************************************
* Public flags
*/
bool FSM::flag_configRequest = false;			/**< initializes the flag_configRequest to false (no config request) */
bool FSM::flag_frequenciesReady = false;		/**< initializes the flag_frequenciesReady to false (no measurement to be read) */
bool FSM::flag_measure = false;				/**< initializes the flag_measure to false (no measurement to start) */
bool FSM::flag_SD_record = false;				/**< initializes the flag_SD_record to false (no measurement to start) */

/******************************************************************************
* State machine mechanic methods
*/
bool FSM::init_sd()
{
	// if (!card.init(SPI_HALF_SPEED, SD_CS)) {
	//     Serial.println("initialization failed. Things to check:");
	//     Serial.println("* is a card inserted?");
	//     Serial.println("* is your wiring correct?");
	//     Serial.println("* did you change the chipSelect pin to match your shield or module?");
	// 	digitalWrite(SD_LED, HIGH);
	//   } else {
	//     Serial.println("Wiring is correct and a card is present.");
	//   }

			// if sd is not initialize, do it
	Serial.print("Initializing SD card...");
	// make sure that the default chip select pin is set to
	//		see if the card is present and can be initialized:
	if (!SD.begin(SD_CS)) {
		Serial.println("Card failed, or not present");
		// don't do anything more:
		return false;
	}
	else {
		Serial.println("card initialized.");
		return true;
	}
}

/// todo bug in the FSM, if no anemo, never the averages are calculated... loop around st_measure
// this method initialize the FSM
void FSM::init(){

#ifdef DEBUG_EEPROM
	// for(int i= 0; i <1024; ++i){
	// 	eeprom_write_byte(i, 254);
	// }

	// #ifdef DEBUG_FSM
	// Serial.println(" ");
	// for(int i= 1; i <1024; ++i){
	// 	if((i%10)==9){
	// 		Serial.println(eeprom_read_byte(i));
	// 	}
	// 	else{
	// 		Serial.print(eeprom_read_byte(i));
	// 		Serial.print(' ');
	// 	}
	// }
	// Serial.println(" ");
	// #endif
#endif


	m_eeprom_addr = 0;				// data are store from the 0 adress onwards
	nextState = &FSM::st_SETUP;		// the first state is : st_setup

	load_param();					// calls the method that loads the values of the FSM parameters from EEPROM

	rtc.getDateTime();				// calls the method that uploads date and time from the RTC
	monthOld = rtc.getMonth();
	Serial.println(rtc.formatDate());
	Serial.println(rtc.formatTime());


	pinMode(SD_CS, OUTPUT);	// define SD_CS as an output pin
	digitalWrite(SD_CS, HIGH);
	pinMode(SD_RECORD_PIN,INPUT);
	pinMode(SD_LED, OUTPUT);
	digitalWrite(SD_CS, HIGH);


	anemo1.init();            // calls the method that loads the parameters of the anemometer 1
	anemo2.init();            // calls the method that loads the parameters of the anemometer 2
	//
	vane.load_param();              // calls the method that loads the parameters of the wind vane

	power1.init();					// calls the method thatinitialize and loads the parameters of the power measurement 1 (DC or AC)
	power2.load_param();            // calls the method that loads the parameters of the power measurement 2 (DC or AC)
	power3.load_param();            // calls the method that loads the parameters of the power measurement 3 (DC or AC)
	power4.load_param();            // calls the method that loads the parameters of the power measurement 4 (DC or AC)

	SD_mem_id = 0;

	#ifdef DEBUG_EEPROM
	Serial.println("After init");
	for(int i= 0; i <1024; ++i){
		if((i%10)==9){
			Serial.println(eeprom_read_byte(i));
		}
		else{
			Serial.print(eeprom_read_byte(i));
			Serial.print(' ');
		}
	}
	Serial.println(" ");
	#endif

	adc.init();
}

void FSM::timingControl(){
	//update local variables
	rtc.getTime();

	if(secondOld!=rtc.getSecond()){				// is it a new second ?
		second_counter++;						// Use a second counter to compare with the measurePeriode

		#ifdef DEBUG_FSM
		Serial.print("second_counter : "); Serial.print(second_counter);
		Serial.print(" measurePeriode : "); Serial.print(measurePeriode);
		#endif

		if((second_counter%measurePeriode)==0)
		{
			#ifdef DEBUG_FSM
			Serial.println(" New measure.");
			#endif

			flag_measure = true;				// if it's true, we can do a new measure
			second_counter=0;					// reset the counter
		}
		secondOld = rtc.getSecond();		// update timestamp_old
	}

	// get anemo flag value
	flag_frequenciesReady = anemo1.flag_anemo();
	// Serial.print(anemo1.flag_anemo());
	// get SD record flag
	flag_SD_record = digitalRead(SD_RECORD_PIN);
}

/******************************************************************************
* Configuration management
******************************************************************************/
//--------------------------------------------------------------------------------------------------------------------------------------
// METHOD: menu
// Decription:  Prints the commands of the configuration menu
//--------------------------------------------------------------------------------------------------------------------------------------
void FSM::menu(){
	Serial.println("Configuration menu :");
	Serial.println("	$1 - General");
	Serial.println("	$2 - Date/Time");
	Serial.println("	$3 - Anemo1");
	Serial.println("	$4 - Anemo2");
	Serial.println("	$5 - Windvane");
	Serial.println("	$6 - Power1");
	Serial.println("	$7 - Power2");
	Serial.println("	$8 - Power3");
	Serial.println("	$9 - Power4");

	Serial.println("	$0 - Output configuration");
}

void FSM::printConfig(){
	Serial.println("FSM config :");
	Serial.print("  *11 measure_sample_conf = ");	Serial.println(measureSampleConf);
	Serial.println("		0: no measure, 1: 10s average,  2:1min average,\r\n		3:10min average, 4:no average (each second)");
	Serial.print("  *12 node id = ");	Serial.println(node_id);
	Serial.println("		permit to identify each datalogger (0 - 255).");
	Serial.println("  *19 Reset configuration - reinitialize the FSM and each sensors configuration.");
	Serial.print("\r\n  info : SupplyVoltage = ");	Serial.print(adc.getVcc());Serial.println("V");
	Serial.println("		autosetted from 2.5V reference.");

}

void FSM::printDateTime(){
	Serial.println("Date/Time config :");
	Serial.print("	*21 Time : ");	Serial.println(rtc.formatTime());
	Serial.print("	*22 Date : ");	Serial.println(rtc.formatDate());
}

bool FSM::config(char *stringConfig){
	uint8_t item = stringConfig[2]-'0';	// convert item in char

	double arg_f = atof(stringConfig + 4);	// convert the second part, the value in double to cover all possibilities.
	unsigned char arg_uc = (unsigned char)arg_f;
	switch (item) {
		case 1:	// choose measurement periode parameter
		switch (arg_uc) {
			case 0:	// no measurement, use at the first wake up
			measureSampleConf = 0;measureMax = 0;measurePeriode = 0;
			update_param();	measure = 0; second_counter = 0;
			break;
			case 1:	// Config 1 : 2 measures in 10 secondes
			measureSampleConf = 1;measureMax = 2;measurePeriode = 5;
			update_param();	measure = 0; second_counter = 0;
			// Set timer1 for small period
			anemo1.set_prescaler(5);
			anemo2.set_prescaler(5);
			break;
			case 2:	// Config 2 : 4 measures in 1 minute
			measureSampleConf = 2;measureMax = 4;measurePeriode = 15;
			update_param();	measure = 0; second_counter = 0;
			anemo1.set_prescaler(5);
			anemo2.set_prescaler(5);
			break;
			case 3:	// Config 3 : 10 measures in 10 minutes
			measureSampleConf = 3;measureMax = 10;measurePeriode = 60;
			update_param();	measure = 0; second_counter = 0;
			anemo1.set_prescaler(5);
			anemo2.set_prescaler(5);
			break;
			case 4:	// Config 4 : 1 measures in 1 second
			measureSampleConf = 4;measureMax = 1;measurePeriode = 1;
			update_param();	measure = 0; second_counter = 0;
			anemo1.set_prescaler(3);
			anemo2.set_prescaler(3);
			break;
			default:
			Serial.println("Not a correct value");
			break;
		}
		break;
		case 2:	// Set the Node id number
		node_id = arg_uc;
		update_param();
		break;
		case 8:	// Set the Node id number
		SupplyVoltage = arg_f;
		update_param();
		break;
		case 9:	// Reset each param,add here each sensors initialise_param method
		initialize_param();		// initialize the FSM
		anemo1.initialize_param();
		anemo2.initialize_param();
		vane.initialize_param();
		power1.initialize_param();
		power2.initialize_param();
		power4.initialize_param();
		power3.initialize_param();
		break;
		default:
		Serial.print("Bad request : ");Serial.println(item);
	}

	#ifdef DEBUG_EEPROM
	Serial.println(" ");

	for(int i= 0; i <1024; ++i){
		if((i%10)==9){
			Serial.println(eeprom_read_byte(i));
		}
		else{
			Serial.print(eeprom_read_byte(i));
			Serial.print(' ');
		}
	}
	Serial.println(" ");
	#endif

	return 0;
}

void FSM::configDT(char *stringConfig){
	// valid string are :
	//	*21=hh:mm:ss
	//	*22=mm/dd/yyyy
	//	else bad request
	uint8_t item = stringConfig[2]-'0';	// convert item in char

	switch (item) {
		case 1:	//	*21=hh:mm:ss
		if(stringConfig[6]==':'&&stringConfig[9]==':'){	// test time separator
			char hours[3]={stringConfig[4],stringConfig[5],'\0'};
			char mins[3]={stringConfig[7],stringConfig[8],'\0'};
			char secs[3]={stringConfig[10],stringConfig[11],'\0'};

			rtc.setDateTime(rtc.getDay(), rtc.getWeekday(), rtc.getMonth(), 0,  rtc.getYear(), atoi(hours), atoi(mins), atoi(secs));
		}
		else Serial.println("Bad value : type *21=hh:mm:ss");
		break;
		case 2://	*22=mm/dd/yyyy
		if(stringConfig[6]=='/'&&stringConfig[9]=='/'){	// test date separator
			char months[]={stringConfig[4],stringConfig[5],'\0'};
			Serial.print("Months : "); Serial.println(months);
			char days[]={stringConfig[7],stringConfig[8],'\0'};
			char years[]={stringConfig[12],stringConfig[13],'\0'};
			byte month = atoi(months);
			Serial.print("Month : "); Serial.println(month);

			rtc.setDateTime(atoi(days), rtc.getWeekday(), month, 0, atoi(years), rtc.getHour(), rtc.getMinute(), rtc.getSecond());
		}
		else Serial.println("Bad value : type *22=mm/dd/yyyy");
		break;
		default:
		Serial.print("Bad request : ");Serial.print(item);
		break;
	}
}

void FSM::configOutput(char *stringConfig){
	uint8_t item = stringConfig[2]-'0';	// convert item in char

	double arg_f = atof(stringConfig + 4);	// convert the second part, the value in double to cover all possibilities.
	unsigned char arg_uc = (unsigned char)arg_f;
	switch (item) {
		case 1:	// enable or disable write data on Serial
		if(arg_uc==0)serial_enable = false;	// disable
		else serial_enable = true;				// enable
		update_param();
		break;
		case 2:	// enable or disable write data on SD card
		if(arg_uc==0)sd_enable = false;	// disable
		else sd_enable = true;				// enable
		update_param();
		break;
		//			case 3:	// Set offset value
		//				m_offset = arg_f;
		//				update_param();
		//				break;
		default:
		Serial.print("Bad request : ");Serial.println(item);
	}
}

//--------------------------------------------------------------------------------------------------------------------------------------
// METHOD: printOutput
// Decription:  Prints information about the enabled output of the datalogger. Add new output peripherals here.
//--------------------------------------------------------------------------------------------------------------------------------------
void FSM::printOutput(){
	Serial.println("Output config :");                                      // prints the title
	Serial.print("	*01 Serial enable : ");	Serial.println(serial_enable);  // prints if the serial is enabled
	Serial.print("	*02 Sd card enable : ");	Serial.println(sd_enable);  // prints if the sd card is enabled
}

/******************************************************************************
* State list declaration
*/

//--------------------------------------------------------------------------------------------------------------------------------------
// METHOD: st_SETUP
// Decription:  Executes the initial setup of the datalogger
//--------------------------------------------------------------------------------------------------------------------------------------
void FSM::st_SETUP(){
	#ifdef DEBUG_FSM
	Serial.println("st_SETUP");
	#endif

	// by default the transition is ev_waiting
	ev_isWaiting();         // This event is a flag that calls the st_SLEEP state
}

//--------------------------------------------------------------------------------------------------------------------------------------
// METHOD: st_CONFIG
// Decription:  Runs the config state
// Transition priorities:   1       | 2
//                          config  | sleep
//--------------------------------------------------------------------------------------------------------------------------------------
void FSM::st_CONFIG(){

	#ifdef DEBUG_FSM
	//Serial.println("st_CONFIG");
	#endif

	if(flag_configRequest==true){
		Serial.println(serialString);

		switch (serialString[0]) {
			case '$':                       // A "$" sign denotes a reading procedure from the menu.
			// menu config
			switch (serialString[1]) {
				case '$':               // A second $ sign denotes a change in the settings
				isInConfig = 1;		// Sets inInConfig flag to one, keeping the datalogger in the same state
				menu();             // Prints the commands for the configuration menu
				break;
				case '1':               // a "1" calls the print of the current config values
				printConfig();
				break;
				case '2':
				printDateTime();    // a "2" calls the print of the date and time values
				break;
				case '3':
				anemo1.print_config();  // a "3" calls the print of the anemometer 1 configuration values
				break;
				case '4':
				anemo2.print_config();  // a "4" calls the print of the anemometer 2 configuration values
				break;
				case '5':
				vane.print_config();    // a "5" calls the print of the wind vane configuration values
				break;
				case '6':
				power1.print_config();  // a "6" calls the print of the configuraiton values for the power calculation 1
				break;
				case '7':
				power2.print_config();  // a "7" calls the print of the configuration values for the power calcualtion 2
				break;
				case '8':
				power3.print_config();  // a "7" calls the print of the configuration values for the power calcualtion 2
				break;
				case '9':
				power4.print_config();  // a "7" calls the print of the configuration values for the power calcualtion 2
				break;
				case '0':
				printOutput();              // a "9" calls the print of the Output values
				break;
				case 'q':                   // a "q" closes the configuration and starts the measurements
				Serial.println();
				Serial.println("Config Done and start measurement.");
				isInConfig = 0;	                                        // desables the flag that shows the code is in configuration mode
				measure = 0; second_counter = 0;	                    // resets the measurements and the counter of secondsmeasure
				SD_mem_id = 0;
				break;
				default:                    // anything else gets a return as a "bad request"
				Serial.println("Bad request");
				break;
			}
			break;
			case '*':              // a "*" denotes a writing routine into the config values
			// Sub Config sender
			switch (serialString[1]) {
				case '1':                   // a "1" denotes writing the data onto the config
				config(serialString);
				printConfig();
				break;
				case '2':                   // a "2" denotes writing onto the date and time config
				configDT(serialString);
				printDateTime();
				break;
				case '3':                           // a "3" denotes writing onto the anemometer 1 config
				anemo1.config(serialString);
				anemo1.print_config();
				break;
				case '4':                           // a "4" denotes writing onto the anemometer 2 config
				anemo2.config(serialString);
				anemo2.print_config();
				break;
				case '5':                           // a "5" denotes writing onto the wind vane config
				vane.config(serialString);
				vane.print_config();
				break;
				case '6':                           // a "6" denotes writing onto the power 1 config
				power1.config(serialString);
				power1.print_config();
				break;
				case '7':                           // a "7" denotes writing onto the power 2 config
				power2.config(serialString);
				power2.print_config();
				break;
				case '8':                           // a "7" denotes writing onto the power 2 config
				power3.config(serialString);
				power3.print_config();
				break;
				case '9':                           // a "7" denotes writing onto the power 2 config
				power4.config(serialString);
				power4.print_config();
				break;

				case '0':                           // a "9" denotes writing onto the output config (serial, sd, wifi, etc...)
				configOutput(serialString);
				printOutput();
				break;
				default:                            // anything else is seen as a "bad request"
				Serial.println("Bad request");
				break;

			}
			break;
			default:
			Serial.println("Bad request");          // if neither a "$" nor a "*" is given, the code returns a "bad request"
			break;
		}
		StrIndex=0;                                     // resets the string index
		flag_configRequest = false;                     // resets the config request flag

		if(!(serialString[0]=='$' && serialString[1]=='q' ))	                    // reminds the user how to quit ($q) or how to keep configuring ($$)
		Serial.println("Main menu : $$ or Quit and start measurement : $q");
	}

	//--------------------------------------------------
	// Transition tests
	//--------------------------------------------------

	if(flag_configRequest || isInConfig>0) ev_configRequest();          // stays in the config status is still active, it loops (triggers the ev_configRequest that calls the st_CONFIG)
	else ev_isWaiting();	                                            // by default the transition is ev_waiting

}

//--------------------------------------------------------------------------------------------------------------------------------------
// METHOD: st_SLEEP
// Decription:  Runs the sleep state
// Transition priorities:   1       | 2         | 3                 | 4
//                          config  | measure   | read_frequencies  | sleep
//--------------------------------------------------------------------------------------------------------------------------------------
void FSM::st_SLEEP(){
	#ifdef DEBUG_FSM
	//Serial.println("st_SLEEP");
	#endif


	//--------------------------------------------------
	// Transition tests
	//--------------------------------------------------
	if(flag_configRequest) ev_configRequest();              // Priority 1 - this event calls the st_CONFIG state

	else if(flag_frequenciesReady)ev_frequenciesReady();    // Priority 3 - this event calls the st_READ_FREQUENCIES state
	else if(flag_measure) {
		ev_measure();                                       // Priority 2 - this event calls the st_MEASURE state
	}
	else ev_isWaiting();	                                // Priority 4 - (default) this event calls the st_SLEEP state (loops)
}

void FSM::st_MEASURE(){

	#ifdef DEBUG_FSM
	Serial.println("st_MEASURE");
	#endif
	digitalWrite(LED_BUILTIN,HIGH);			// led on

	if(anemo1.is_enable()||anemo2.is_enable()) anemo1.start();	// start anemo1 and 2

	if(vane.is_enable()) vane.read_value(measure);				// read the windvane value
	//
	if(power1.is_enable())power1.read_value(measure, 4);		// read power value
	if(power2.is_enable())power2.read_value(measure, 4);
	if(power3.is_enable())power3.read_value(measure, 4);
	if(power4.is_enable())power4.read_value(measure, 4);

	digitalWrite(LED_BUILTIN,LOW);			// led off

	// reset the flag
	flag_measure = false;

	//--------------------------------------------------
	// Transition tests
	//--------------------------------------------------
	if(flag_configRequest) ev_configRequest();                              // calls the config state
	else if(flag_frequenciesReady)ev_frequenciesReady();
	else if(!(anemo1.is_enable())&&(!anemo2.is_enable())) ev_testCounter();
	else ev_isWaiting();													// by default the transition is ev_waiting
}

void FSM::st_READ_FREQUENCIES(){
	#ifdef DEBUG_FSM
	Serial.println("st_READ_FREQUENCIES");
	#endif

	if(anemo1.is_enable())anemo1.read_value(measure);
	if(anemo2.is_enable())anemo2.read_value(measure);

	flag_frequenciesReady = false;

	// Transition test ?
	if(flag_configRequest) ev_configRequest();
	else ev_testCounter();
}

void FSM::st_CALC_AVERAGES(){
	#ifdef DEBUG_FSM
	Serial.println("st_CALC_AVERAGES");
	#endif

	measure++;				// increase measure

	// test measure number, if equal measureMax, it's time to made average
	bool isMeasureMax = false;
	if(measure == measureMax){
		isMeasureMax = true;

		anemo1.calc_average(measureMax);
		anemo2.calc_average(measureMax);
		vane.calc_average(measureMax);
		power1.calc_average(measureMax);
		power2.calc_average(measureMax);
		power3.calc_average(measureMax);
		power4.calc_average(measureMax);

		timestamp = rtc.getTimestamp();	// save average's timestamp
		measure = 0;	// restart a new sequence

		anemo1.clear(measureMax);
		anemo2.clear(measureMax);
		vane.clear(measureMax);
		power1.clear(measureMax);
		power2.clear(measureMax);
		power3.clear(measureMax);
		power4.clear(measureMax);
	}

	//--------------------------------------------------
	// Transition tests
	//--------------------------------------------------

	if(flag_configRequest) ev_configRequest();
	else if(isMeasureMax)ev_transmitting();
	else ev_isWaiting();
}

void FSM::serial_print(){
	char dataString[50];

	// print on Serial (uart 0)
	Serial.print(timestamp); Serial.print("	");
	Serial.print(node_id); 	Serial.print("	");
	if(anemo1.is_enable())	{Serial.print(anemo1.get_average()); Serial.print("	");}
	if(anemo2.is_enable())	{Serial.print(anemo2.get_average()); Serial.print("	");}
	if(vane.is_enable())	{Serial.print(vane.get_average());	Serial.print("	");}
	if(power1.is_enable())	{Serial.print(power1.get_average(dataString));	Serial.print("	");}
	if(power2.is_enable())	{Serial.print(power2.get_average(dataString));	Serial.print("	");}
	if(power3.is_enable())	{Serial.print(power3.get_average(dataString));	Serial.print("	");}
	if(power4.is_enable())	{Serial.print(power4.get_average(dataString));}
	Serial.println();
}

void FSM::sd_build_string(){
	// todo sd led modes
	char tempString[50];
	char (*ptr_dataString) = &(SD_mem[SD_mem_id][0]);

	strcpy(ptr_dataString, ltoa(timestamp, tempString, 10)); strcat(ptr_dataString,"	");
	strcat(ptr_dataString,itoa(node_id, tempString, 10)); strcat(ptr_dataString,"	");
	if(anemo1.is_enable())	{strcat(ptr_dataString,dtostrf(anemo1.get_average(), 1, 1, tempString)); strcat(ptr_dataString,"	");}
	if(anemo2.is_enable())	{strcat(ptr_dataString,dtostrf(anemo2.get_average(), 1, 1, tempString)); strcat(ptr_dataString,"	");}
	if(vane.is_enable())	{strcat(ptr_dataString,itoa(vane.get_average(), tempString, 10)); strcat(ptr_dataString,"	");}
	if(power1.is_enable())	{strcat(ptr_dataString,power1.get_average(tempString)); strcat(ptr_dataString,"	");}
	if(power2.is_enable())	{strcat(ptr_dataString,power2.get_average(tempString)); strcat(ptr_dataString,"	");}
	if(power3.is_enable())	{strcat(ptr_dataString,power3.get_average(tempString)); strcat(ptr_dataString,"	");}
	if(power4.is_enable())	{strcat(ptr_dataString,power4.get_average(tempString)); strcat(ptr_dataString,"	");}
}

void FSM::sd_filename(){
	// set file name from year and month
	char tempConv[6];
	strcpy(fileName,itoa(rtc.getMonth(),tempConv,10));
	strcat(fileName,"_");
	strcat(fileName,itoa(rtc.getYear(),tempConv,10));
	strcat(fileName,".csv");
}

void FSM::st_OUTPUT(){
	#ifdef DEBUG_FSM
	Serial.println("st_OUTPUT");
	#endif
	bool isTransmitting = true;

	if(serial_enable==true){
		serial_print();
	}

	if(sd_enable==true){

		//Serial.println("SD output :");

		sd_build_string();	// write the string

		//Serial.println("String builded.");

		SD_mem_id++; // increment index
		//Serial.print("SD mem id : ");Serial.println(SD_mem_id);

		if((rtc.getMonth() != monthOld) || flag_SD_record || (SD_mem_id >= 20)){
			Serial.print("SD initialisation...");
			//init_sd();	// test initialization

			// Now we will try to open the 'volume'/'partition' - it should be FAT16 or FAT32
			if(!SD.begin(SD_CS))	// sd card is initialize, write on
			{
    			Serial.println("Card failed, or not present");
			}
			else{
				Serial.println("card initialized.");
				//digitalWrite(SD_CS, HIGH);
				sd_filename();

				// open the file. note that only one file can be open at a time,
				// so you have to close this one before opening another.
				myFile = SD.open(fileName, FILE_WRITE);

				// if the file is available, write to it:
				if (myFile) {
					for(unsigned int i=0; i<=SD_mem_id; i++){
						char (*ptr_dataString) = &(SD_mem[i][0]);
						myFile.println(ptr_dataString);
					}
					Serial.println("Data writed");
					myFile.close();
				}
				else{
					Serial.print("error opening "); Serial.println(fileName);
				}
				digitalWrite(SD_CS, HIGH);
			}
			SD_mem_id = 0;
		}

		if(flag_SD_record == true)
		digitalWrite(SD_LED, LOW);
		flag_SD_record = false;
	}


	monthOld=rtc.getMonth();	// save the new day

	isTransmitting = false;

	// Transition test ?
	if(isTransmitting) ev_transmitting();
	else ev_isWaiting();
}


/******************************************************************************
* Eeprom management
*/
//--------------------------------------------------------------------------------------------------------------------------------------
// METHOD: load_param
// Decription:  Loads information from the EEPROM. If there's an incompatibility between the structure version and the data
//              structure version, then it calls the initialize_param() method.
//--------------------------------------------------------------------------------------------------------------------------------------
void FSM::load_param(){
	structure_version = eeprom_read_byte((const unsigned char*)EE_DATA_STRUCT);                  // loads the current version of the code
	if(structure_version != DATA_STRUCTURE_VERSION) initialize_param();                         // checks that the curent data structure is the same as the current version of the code
	else{
		node_id = eeprom_read_byte((const unsigned char*)(unsigned int)EE_FSM_ID);                      // loads the node_id parameter - the id of the datalogger
		measureSampleConf = eeprom_read_byte((const unsigned char*)EE_MSC);            // loads the measureSambleConf parameter - defines the sample rate (1 second, 1 minute, 10 minutes)
		measureMax = eeprom_read_byte((const unsigned char*)EE_MM);                   // loads the measureMax parametre - defines the maximum number
		measurePeriode = eeprom_read_byte((const unsigned char*)EE_MP);              // loads the measureMax parametre - defines the measurement rate in seconds

		if(eeprom_read_byte((const unsigned char*)EE_SERIAL_E)==0) serial_enable = false;  // loads the serial_enable paramter - turns on the serial port. Since this is a boolean, it is loaded through an if that sets a boolean variable
		else serial_enable = true;
		if(eeprom_read_byte((const unsigned char*)EE_SD_E)==0) sd_enable = false;      // loads the sd_enable paramter - turns on the SD card. Since this is a boolean, it is loaded through an if that sets a boolean variable
		else sd_enable = true;
		SupplyVoltage = eeprom_read_float((float*)EE_VCC);

	}
}

//--------------------------------------------------------------------------------------------------------------------------------------
// METHOD: update_param
// Decription:  Updates information onto the EEPROM.
//--------------------------------------------------------------------------------------------------------------------------------------
void FSM::update_param (){
	eeprom_update_byte((unsigned char*)EE_DATA_STRUCT, structure_version);                   // writes the structure_version parameter - the value of the data structure version
	eeprom_update_byte((unsigned char*)EE_FSM_ID, node_id);                           // writes the node_id parameter -
	eeprom_update_byte((unsigned char*)EE_MSC, measureSampleConf);                 // writes the measureSampleConf parameter
	eeprom_update_byte((unsigned char*)EE_MM, measureMax);                        // writes the measureMax parameter
	eeprom_update_byte((unsigned char*)EE_MP, measurePeriode);                   // writes the measurePeriod parameter

	if(serial_enable==false) eeprom_update_byte((unsigned char*)EE_SERIAL_E, 0);	// writes the serial_enable parameter - Since this is a boolean parameter, it is written through an if. 0 if the serial is off and 1 if the serial is on.
	else eeprom_update_byte((unsigned char*)EE_SERIAL_E, 1);

	if(sd_enable==false) eeprom_update_byte((unsigned char*)EE_SD_E, 0);	    // writes the sd_enable parameter - Since this is a boolean parameter, it is written through an if. 0 if the sd is off and 1 if the sd is on.
	else eeprom_update_byte((unsigned char*)EE_SD_E, 1);

	eeprom_update_float((float*)EE_VCC, SupplyVoltage);

}

//--------------------------------------------------------------------------------------------------------------------------------------
// METHOD: initialize_param
// Decription:  Initialize the eeprom memory. This method is called automatically whenever there's an incompatibility between the
//              structure version and the data structure version
//--------------------------------------------------------------------------------------------------------------------------------------
void FSM::initialize_param (){
	eeprom_write_byte((unsigned char*)EE_DATA_STRUCT, DATA_STRUCTURE_VERSION);          // writes the structure_version parameter - the value of the data structure version
	eeprom_write_byte((unsigned char*)EE_FSM_ID, 15);                            // writes the node_id parameter - sets the id value that identifies the datalogger as 15 by default
	eeprom_write_byte((unsigned char*)EE_MSC, 0);                             // writes the measureSampleConf parameter - sets the measurement timing as 0 by default
	eeprom_write_byte((unsigned char*)EE_MM, 0);                             // writes the measureMax parameter - sets the maximum number of measurements to be performed by the code as 0 by default
	eeprom_write_byte((unsigned char*)EE_MP, 0);                            // writes the measurePeriod parameter - defines the interval between two measurements in seconds
	eeprom_write_byte((unsigned char*)EE_SERIAL_E, 1);                            // writes the serial_enable parameter - sets the serial as active by default
	eeprom_write_byte((unsigned char*)EE_SD_E, 0);                            // writes the sd_enable parameter - sets the sd card as off by default
	eeprom_write_float((float*)EE_VCC, 5000);                            		// supplyVoltage

	load_param();
}
